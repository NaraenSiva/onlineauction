$(document).ready(function(){
    $('#search').autocomplete({
        source: function(request, response){
            $.ajax({
                url: "<?php echo base_url("papers/search");?>",
                data: {
                    term: request.term
                },
                dataType: "json",
                success: function(data){
                    var res = $.map(data, function(obj){
                        return obj.name;
                    });
                    response(resp);
                }
            }),
        };
        minLength: 1
    });
});
<?php
class User_model extends CI_Model{
    public function getData($userid){
        $this->db->where('id', $userid);
        $query = $this->db->get('users');
        return $query->result();
    }

    public function updateUsername($username, $userid){
        $data = array(
            'username' => $username
        );
        $this->db->where('id', $userid);
        $this->db->update('users', $data);
    }

    public function updateEmail($email, $userid){
        $data = array(
            'email' => $email
        );
        $this->db->where('id', $userid);
        $this->db->update('users', $data);
    }

    public function updatePassword($password, $userid){
        $data = array(
            'password' => $password
        );
        $this->db->where('id', $userid);
        $this->db->update('users', $data);
    }

    public function retrieveWatchList($userid){
        $this->db->select('productID');
        $this->db->where('userID', $userid);
        $query = $this->db->get('watchlist');
        return $query->result();
    }

    public function getProductInfo($productID){
        $this->db->where('id', $productID);
        $query = $this->db->get("products");
        return $query->result();
    }

    public function deleteItem($userID, $productID){
        $this->db->where('productID', $productID);
        $this->db->where('userID', $userID);
        $this->db->delete('watchlist');
    }

    public function getProductsByOwner($userid){
        $this->db->where('ProductOwnerID', $userid);
        $query = $this->db->get('products');
        return $query->result_array();
    }

    public function getProducts(){
        $query = $this->db->get('products');
        return $query->result_array();
    }

    public function getSpecificNotifications($userid){
        $this->db->where('toid', $userid);
        $this->db->where('comment', 0);
        $this->db->where('subject', "Item Expired");
        $query = $this->db->get('notifications');
        return $query->result_array();
    }

    public function removeItem($productID){
        $this->db->where('id', $productID);
        $this->db->delete('products');
    }

    public function getHighestBidder($productID){
        $query = $this->db->query("SELECT userID, MAX(bidAmount) as bidAmount FROM bid WHERE productId = '$productID' GROUP BY userID");
        if ($query->num_rows()){
            return $query->result();
        } else {
            return false;
        }
    }

    public function getItems($value){
        $this->db->like('productName', $value);
        $query = $this->db->get('products');
        return $query->result();
    }

    public function removeItemWatchlist($productID){
        $this->db->where('productId', $productID);
        $this->db->delete('watchlist');
    }
}
?>
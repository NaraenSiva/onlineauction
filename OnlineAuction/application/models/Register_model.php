<?php
class Register_model extends CI_Model{
    function insert($data){
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    function verify($token){
        $this->db->where('token', $token);
        $this->db->where('email_verified', false);
        $query = $this->db->get('users');
        if($query->num_rows() > 0){
            $data = array(
                'email_verified' => '1'
            );

            $this->db->where('token', $token);
            $this->db->update('users', $data);
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    public function existingUsername($username){
        $this->db->where('username', $username);
        $query = $this->db->get('users');
        return $query->num_rows();
    }

    public function existingEmail($email){
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        return $query->num_rows();
    }
}
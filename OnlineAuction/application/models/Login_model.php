<?php
class Login_Model extends CI_Model{
    public function validation($username, $password){
        $this->db->where('username', $username);
        $this->db->where('email_verified', true);
        $query = $this->db->get('users');
        if ($query->num_rows() >0){
            foreach($query->result() as $row){
                $db_password = $row->password;
                $cookiePass = $this->getCookiePass($username);
                if (password_verify($password,$db_password)){
                    $this->session->set_userdata('id', $row->id);
                    return '';
                } else{
                    return 'Wrong Password!';
                }
            }
        }
        else{
            return 'Enter a valid username!';
        }
    }

    public function getCookiePass($username){
        $this->db->where('username', $username);
        $query = $this->db->get('token');
        foreach($query->result() as $row){
            $value = $row->encryptPassword;
        }
        return $value;
    }

    public function removeToken($username){
        $this->db->where('username', $username);
        $this->db->delete('token');
    }

    public function existsToken($username){
        $this->db->where('username', $username);
        $query = $this->db->get('token');
        if ($query->num_rows() > 0){
            return false;
        } else{
            return true;
        }
    }

    public function pwdRowDelete($email){
        $this->db->where('passwordResetEmail', $email);
        $query = $this->db->get("password_reset");
        if ($query->num_rows() > 0){
            $this->db->where('passwordResetEmail', $email);
            $this->db->delete("password_reset");
        }
        return true;
    }

    public function emailExists($email){
        $this->db->where('email', $email);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0){
            return true;
        }
        return false;
    }
    
    public function insert($data){
        $this->db->insert("password_reset", $data);
        return $this->db->insert_id();
    }

    public function findRow($selector, $currentDate){
        $this->db->where("passwordResetSelector", $selector);
        $this->db->where("passwordResetExpiry >=", $currentDate);
        $query = $this->db->get("password_reset");
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    public function getRowToken($selector, $currentDate){
        $this->db->where("passwordResetSelector", $selector);
        $this->db->where("passwordResetExpiry >=", $currentDate);
        $query = $this->db->get("password_reset");
        if ($query->num_rows() > 0){
            $row = $query->row();
            return $row->passwordResetToken;
        }
    }

    public function getRowEmail($selector, $currentDate){
        $this->db->where("passwordResetSelector", $selector);
        $this->db->where("passwordResetExpiry >=", $currentDate);
        $query = $this->db->get("password_reset");
        if ($query->num_rows() > 0){
            $row = $query->row();
            return $row->passwordResetEmail;
        }
    }



    public function updatePwd($password, $email){
        if ($this->emailExists($email)){
            $data = array(
                'password' => $password
            );
            $this->db->where('email', $email);
            $this->db->update('users', $data);
            return true;
        }
        return false;
    }

    public function logout($userid){
        $this->db->where('user_id', $userid);
        $this->db->delete('login_details');
    }

    public function addToken($username, $password){
        $data = array(
            'username' => $username,
            'encryptPassword' => $password
        );

        $this->db->insert('token', $data);
    }
}

?>
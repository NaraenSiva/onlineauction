<?php
class Search_Model extends CI_Model{
    public function search($mainEntry, $category){
        $this->db->like('productName', $mainEntry);
        if ($category != null){
            $this->db->where('productType', $category);
        }
        $query = $this->db->get('products');
        return $query;
    }
}
?>

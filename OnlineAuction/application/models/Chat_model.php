<?php
class Chat_model extends CI_Model{
    public function findFriends($userid){
        $this->db->where('user_id', $userid);
        $this->db->select('friend_id');
        $query = $this->db->get('friends');
        return $query;
    }

    public function getInfo($userid){
        $this->db->where('id', $userid);
        $this->db->select('id, username');
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function setLoginEntrance($userid){
        $data = array(
            'user_id' => $userid
        );
        $this->db->insert('login_details', $data);
        $this->session->set_userdata('login_id', $this->db->insert_id());
    }

    public function getOnlineStatus($userid, $username){
        $this->db->where('user_id', $userid);
        $this->db->order_by('user_id', 'asc');
        $query = $this->db->get('login_details');
        if ($query->num_rows() >0){
            $data = array(
                'username' => $username,
                'id' => $userid,
                'status' => 'True'
            );
            return $data;
        }
        else{
            $data = array(
                'username' => $username,
                'id' => $userid,
                'status' => 'False'
            );
            return $data;
        }
    }

    public function chatMessage($from_user_id, $to_user_id, $message, $timestamp){
        $data = array(
            'to_user' => $to_user_id,
            'from_user' => $from_user_id,
            'message' => $message,
            'timestamp' => $timestamp
        );

        $this->db->insert('chat', $data);
    }

    public function getChatHistory($fromuser, $touser){
        $where = "(from_user = $fromuser AND to_user = $touser) OR (from_user = $touser AND to_user = $fromuser)";
        $this->db->where($where);
        $this->db->order_by('timestamp', 'asc');
        $query = $this->db->get('chat');
        return $query->result_array();
    }

    public function getFromUser($fromUser){
        $this->db->where('id', $fromUser);
        $this->db->select('username');
        $query= $this->db->get('users');
        return $query->result_array();
    }
}
?>
<?php
class Notification_model extends CI_Model{
    public function addNotification($toid, $subject, $comment){
        $data = array(
            'userid' => $_SESSION["user_id"],
            'toid' => $toid,
            'subject' => $subject,
            'comment' => $comment
        );

        $this->db->insert('notifications', $data);
    }

    public function getNotifications(){
        $user = $_SESSION["user_id"];
        $this->db->where('toid', $user);
        $this->db->where('comment', 0);
        $query = $this->db->get('notifications');
        return $query->result();
    }

    public function updateNotifications(){
        $this->db->set('comment', 1);
        $this->db->where('toid', $_SESSION["user_id"]);
        $this->db->update('notifications');
    }

}
?>
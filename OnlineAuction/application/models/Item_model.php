<?php
class Item_model extends CI_Model{
    public function insertNewProduct($data){
        $this->db->insert('products', $data);
        return $this->db->insert_id();
    }
    
    public function comment($message, $id){
        $data = array(
            'product_id' => $id,
            'message' => $message
        );

        $this->db->insert('comment', $data);
        return $this->db->insert_id();
    }

    public function addToWatchList($productid, $userid){
        $data = array(
            'productID' => $productid,
            'userID' => $userid
        );

        $this->db->insert('watchlist', $data);
        return $this->db->insert_id();
    }

    public function getProductInfo($productid){
        $this->db->where('id', $productid);
        $query = $this->db->get('products');
        return $query->result();
    }
    public function getBidHistory($productid){
        $this->db->where('productID', $productid);
        $this->db->order_by('timestamp', 'asc');
        $query = $this->db->get('bid');
        return $query->result();
    }

    public function bid($userid, $productid, $bidamount, $timestamp){
        $data = array(
            'userID' => $userid,
            'productId' => $productid,
            'bidAmount' => $bidamount,
            'timestamp' => $timestamp
        );
        $this->db->insert('bid', $data);
        return $this->db->insert_id();
    }

    public function saveRatings($userid, $productid, $rating){
        $data = array(
            'userID' => $userid,
            'productID' => $productid,
            'rating' => $rating
        );

        $this->db->insert('ratings', $data);
    }

    public function getProduct($productId){
        $this->db->where('id', $productId);
        $query = $this->db->get('products');
        return $query->num_rows();
    }

    public function getFriends($userID, $friendID){
        $where = "(user_id = $userID AND friend_id = $friendID) OR (user_id = $friendID AND friend_id = $userID)";
        $this->db->where($where);
        $query =$this->db->get("friends");
        return $query->num_rows();
    }

    public function addFriends($userID, $friendID){
        $data1 = array(
            'user_id' => $userID,
            'friend_id' => $friendID
        );
        
        $this->db->insert('friends', $data1);

        $data2 = array(
            'user_id' => $friendID,
            'friend_id' => $userID
        );

        $this->db->insert('friends', $data2);
    }

    public function getProductOwnerID($id){
        $this->db->where('id', $id);
        $this->db->select('ProductOwnerID');
        $query = $this->db->get('products');
        return $query->result_array();
    }
}
?>
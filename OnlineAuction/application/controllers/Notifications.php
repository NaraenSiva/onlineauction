<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('notification_model');
    }

    public function addNotification(){
        if (isset($_POST["notification"])){
            if ($_POST["notifications"] != ''){
                $toid = $this->input->post('toid');
                $subject = $this->input->post('subject');
                $comment = $this->input->post('comment');
                $this->notification_model->addNotification($toid, $subject, $comment);
            }
        }
    }

    public function updateNotification(){
        if ($this->input->post('notifications') != ''){
            $this->notification_model->updateNotifications();
        }   
        $query = $this->notification_model->getNotifications();
        $output = '';
        if (count($query) > 0){
            foreach($query as $row){
                $output .= '<li><a>
                            <strong>'.$row->subject.'</strong><br>
                            <small><em>'.$row->comment.'</em></small>
                            </a></li>';
                $count = count($query);
            }
        }
        else{
            $output = '<li><a>No Notifications</a></li>';
            $count = 0;
        }
        $data = array(
            'notifications' => $output,
            'unseen_notifications' => $count
        );
        echo json_encode($data);
    }
}
?>
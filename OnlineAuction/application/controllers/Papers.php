<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Papers extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('user_model');
        $this->load->helper('form');
        $this->load->model('notification_model');
        $this->load->config('email');
        $this->load->library('email');
        $this->load->library('CustomPDF');
    }

    public function index(){
        $this->load->helper('url');
        $data['username'] = $_SESSION["username"];
        $data["user_id"] = $_SESSION["user_id"];
        $this->load->view('templates/header', $data);
        $this->load->view('templates/home');
        $this->load->view('templates/footer');
    }

    public function getDetails(){
        $data['username'] = $_SESSION["username"];
        $data['title'] = "User Profile";
        $this->load->view('templates/header2', $data);
        $userid = $_SESSION["user_id"];
        $data["userInfo"] = $this->user_model->getData($userid);
        $response = $this->user_model->getData($userid);
        foreach($response as $item){
            $image = $item->userAvatar;
        }
        $data["image"] = $image;
        $this->load->view('templates/userprofile', $data);
    }

    public function retrieveDetails(){
        if ($this->input->post('type')==1){
            $username =$this->input->post('username');
            $userid = $_SESSION["user_id"];
            $this->user_model->updateUsername($username, $userid);
            $_SESSION["username"] = $username;
            echo json_encode(array("statusCode"=>200));
        }
        if ($this->input->post('type')==2){
            $email = $this->input->post('email');
            $userid = $_SESSION["user_id"];
            $this->user_model->updateEmail($email, $userid);
            echo json_encode(array("statusCode"=>200));
        }
        if ($this->input->post('type') ==3){
            $password = $this->input->post('password');
            $userid = $_SESSION["user_id"];
            $this->user_model->updatePassword($password, $userid);
            echo json_encode(array("statusCode"=>200));
        }
    }

    public function getWatchList(){
        $data["username"] = $_SESSION["username"];
        $data["title"] = "My Watchlist";
        $userid = $_SESSION["user_id"];
        $itemIDs =$this->user_model->retrieveWatchList($userid);

        $productInfo = array();

        foreach($itemIDs as $items){
            array_push($productInfo, $this->user_model->getProductInfo($items->productID));
        }
        $data["products"] = $productInfo;
        $this->load->view('templates/header2', $data);
        $this->load->view('templates/watchlist', $data);
    }


    public function removeWatchList(){
        if ($this->input->post('type')==1){
            $productId =$this->input->post('productID');
            $userid = $_SESSION["user_id"];
            $this->user_model->deleteItem($userid, $productId);
            echo json_encode(array("statusCode"=>200));
        }
    }

    public function getItems(){
        $data["username"] = $_SESSION["username"];
        $data["title"] = "My Items";
        $query = $this->user_model->getProductsByOwner($_SESSION["user_id"]);

        $productTimes = array();

        foreach($query as $row){
            $expiryTime = $row["currentTime"];

            $date = time("now");
            $newTime = strtotime($expiryTime);
            $diff = $expiryTime - $date;

            if($diff > 0){

                $day = floor($diff / (24 * 3600)); 

                $diff = ($diff % (24 * 3600)); 
                $hour = floor($diff / 3600); 

                $diff %= 3600; 
                $minutes = floor($diff / 60) ; 

                $diff %= 60; 
                $seconds = $diff; 

                $time = "$day days $hour hours $minutes minutes $seconds seconds";
                array_push($productTimes, array($row["id"],$row["productName"], $row["productPrice"], $time));
            }
            else{
                array_push($productTimes, array($row["id"], $row["productName"], $row["productPrice"], "Expired"));
            }
        }   
        $data["times"] = $productTimes;
        $this->load->view('templates/header2', $data);
        $this->load->view('templates/items', $data);
    }

    public function removeItem(){
        if ($this->input->post('type')==8){
            $productId = $this->input->post('productID');
            $highestBidder = $this->user_model->getHighestBidder($productId);
            if ($highestBidder){
                foreach($highestBidder as $row){
                        $bidder = $row->userID;
                }
                $productInfo = $this->user_model->getProductInfo($productId);
                foreach($productInfo as $row){
                        $seller = $row->productOwnerID;
                }
                $bidderInfo = $this->user_model->getData($bidder);
                foreach($bidderInfo as $row){
                    $bidderEmail = $row->email;
                }
                $sellerInfo = $this->user_model->getData($seller);
                foreach($sellerInfo as $info){
                    $sellerEmail = $info->email;
                }
                $pdf = $this->custompdf->getInstance();
                $pdf->getPDFInvoice($productInfo, $bidderInfo, $sellerInfo, $highestBidder);

                $subject = "Receipt for Successful Bid";
                $message = "
                    <p>Hi User</p>
                    <p>We are writing this email to confirm that you have successfully won your bid.</p>
                    <p>First, please read the receipt to begin your payment to the seller and read to make sure the information is correct.</p>
                    <p>Once you have paid, the seller will have the item to you in 3 to 7 business days</p>
                    <p>Kind Regards, </p>
                    <p>The MyBuy Team </p>
                    ";
                $to = $bidderEmail;
                $this->email->set_newline("\r\n");
                $this->email->from('mybuyteam@gmail.com', 'MyBuyTeam');
                $this->email->to($to);
                $this->email->CC($sellerEmail);
                $this->email->attach($_COOKIE["fileName"]);
                $this->email->subject($subject);
                $this->email->message($message);
                if ($this->email->send()){
                    $this->user_model->removeItem($productId);
                    $this->load->view('templates/removeItem-success');
                }
            } else{
                $this->user_model->removeItem($productId);
                $this->load->view('templates/removeItem-check');
            }
        }
    }

    public function checkNotifications(){
        if ($this->input->post('view') == ''){
            $query = $this->user_model->getProducts();
            foreach($query as $row){
                $expiryTime = $row["currentTime"];
                $productOwnerID = $row["productOwnerID"];
                $product = $row["id"];

                $date = time("now");
                $newTime = strtotime($expiryTime);
                $diff = $expiryTime - $date;

                if($diff <= 0){
                    $result = $this->user_model->getSpecificNotifications($productOwnerID);
                    if (count($result) == 0){
                        $this->notification_model->addNotification($productOwnerID, "Item Expired", "One of your products has expired, please check My Items to remove this");
                    }
                }
            }
        }
    }

    public function search(){
        if ($this->input->post('search') == 1){
            if (!empty($this->input->post('value'))){
                $value = $this->input->post('value');
                $query = $this->user_model->getItems($value);
                if (count($query) > 0){
                    print_r("<ul id = 'listitems'>");
                    foreach($query as $record){
                        print_r("<li id = 'notifiy'>$record->productName</li>" );
                    }
                    print_r("</ul>");
                }
            } else{
                print_r("THIS IS NOT WORKING");
            }
        } else{
            print_r("Post didn't work");
        }
    }
}
?>
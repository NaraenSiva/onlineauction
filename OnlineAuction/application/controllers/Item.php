<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('item_model');
        $this->load->model('notification_model');
    }

    public function index(){
        if (isset($_GET["item"])){
            $productId = $this->input->get('id');
            $results = $this->item_model->getProductInfo($productId);
            foreach($results as $result){
                $data["productId"] = $result->id;
                $data["productImage"] = $result->productImage;
                $data["productName"] = $result->productName;
                $data["productCondition"] = $result->productCondition;
                $data["productOwner"] = $result->productOwner;
                $data["productOwnerID"] = $result->productOwnerID;
                $data["productPrice"] = $result->productPrice;
                $data["currentTime"] = $result->currentTime;
                $data['username'] = $_SESSION["username"];
            }
            $data["title"] = "MyBuy Search";

            setcookie('item_id', $productId, time()+5*60);
            $this->load->view('templates/header2', $data);
            $this->load->view('templates/itemdisplay', $data);
            $this->load->view('templates/footer');
        }
    }

    public function seller(){
        $data['username'] = $_SESSION["username"];
        $this->load->view('templates/header1', $data);
        $this->load->view('templates/sellerdisplay');
        $this->load->view('templates/footer');
    }

    public function sellItem(){
        $data["title"] = "Product Upload";
        $data['username'] = $_SESSION["username"];
        $this->load->view('templates/header2', $data);
        $this->load->view('templates/uploadproduct');
        $this->load->view('templates/footer');
    }

    public function uploadProduct(){
        if (isset($_POST['submit-product-items'])){
            $this->form_validation->set_rules('productname', 'Name', 'required');
            $this->form_validation->set_rules('producttype', 'Type', 'required');
            $this->form_validation->set_rules('productcondition', 'Condition', 'required');
            $this->form_validation->set_rules('productprice', 'Price', 'required');
            $this->form_validation->set_rules('productexpiry', 'Expiry', 'required');
            if ($this->form_validation->run()){
                $fileName = $_FILES['picture']['name'];
                $fileTmpName = $_FILES['picture']['tmp_name'];
                $fileSize = $_FILES['picture']['size'];
                $fileError = $_FILES['picture']['error'];
                $fileType = $_FILES['picture']['type'];
    
                $fileExt = explode('.', $fileName);
                $fileActualExt = strtolower(end($fileExt));
    
                $allowed = array('jpg', 'jpeg', 'png');
    
                if (in_array($fileActualExt, $allowed)){
                    if ($fileError === 0){
                        if ($fileSize < 1000000){
                            $fileNameNew = uniqid('', true).".".$fileActualExt;
    
                            $fileDestination = 'uploads/'.$fileNameNew;
                            move_uploaded_file($fileTmpName, $fileDestination);

                            $expiryDate = $this->input->post('productexpiry');
                            $date = time() + ($expiryDate * 3600 * 24);

                            $data = array(
                                'productName' => $this->input->post('productname'),
                                'productType' => $this->input->post('producttype'),
                                'productOwner' => $_SESSION["username"],
                                'productOwnerID' => $_SESSION["user_id"],
                                'productCondition' => $this->input->post('productcondition'),
                                'productPrice' => $this->input->post('productprice'),
                                'currentTime' => $date,
                                'productExpiry' => $this->input->post('productexpiry'),
                                'productImage' => $fileDestination
                            );

                            $this->item_model->insertNewProduct($data);
                            
                            $this->session->set_flashdata('message', "You have succesfully put a Product for Sale!");
                            $this->load->view('templates/productsuccess');
                        }
                        else{
                            $this->session->set_flashdata('message', "This file is too big");
                            $this->sellItem();
                        }
                    }
                    else{
                        $this->session->set_flashdata('message', "There was an error uploading your file!");
                        $this->sellItem();
                    }
                }
                else{
                    $this->session->set_flashdata('message', "You can't upload this file");
                    $this->sellItem();
                }
            }
        }
    }

    public function comment(){  
        $this->load->view('templates/comment');
    }

    public function leaveComment(){
        if(isset($_POST["comment"])){
            $comment = $this->input->post('comment_message');
            $product_id = $this->input->post('item_id');
            $toid = $this->item_model->getProductOwnerID($product_id);
            $query = $this->item_model->comment($comment, $product_id);
            $message = "A User has left you a comment";

            $this->notification_model->addNotification($toid[0]["ProductOwnerID"], "Comment", $message);

            if ($query > 0){
                $this->load->view('templates/comment-success');
                setcookie('item_id', "", time() -3600);
            }
            else{
                // return failure message
            }
        }
    }

    public function watchlist(){
        if (isset($_POST["watchlist"])){
            $productID = $this->input->post('productID');
            $query = $this->item_model->addToWatchList($_COOKIE["item_id"], $_SESSION["user_id"]);
            if ($query > 0){
                $this->session->set_flashdata('message', "Added to Watchlist!");
                $this->load->view('watchlist-success');
            }
        }
    }

    public function reloadBid(){
        if ($this->input->post('type') == 1){
            $html = '';
            $productID = $this->input->post('productId');
            $query = $this->item_model->getBidHistory($productID);
            $data["bids"] = $query;
            $html =$this->load->view('templates/bid', $data, true);
            echo $html;
        } 
    }

    public function bid(){
        if ($this->input->post('type') == 2){
            $bid = $this->input->post('bidvalue');
            $id = $this->input->post('productID');
            $userid = $this->input->post('userID');
            $date = new \DateTime('now', new \DateTimeZone('Australia/Brisbane'));
            $datetimeFormat = 'Y-m-d H:i:s';
            $timestamp = $date->format($datetimeFormat);
    
            $this->item_model->bid($userid, $id, $bid, $timestamp);
            echo json_encode(array("statusCode"=>200));
        }
    }

    public function saveRatings(){
        $userid = $this->input->post('userId');
        $productOwner = $this->input->post('productOwner');
        $productid = $this->input->post('productId');
        $rating = $this->input->post('ratingValue') + 1;

        $this->item_model->saveRatings($userid, $productid, $rating);
        $this->notification_model->addNotification($productOwner, "Comment", "A Person has left a rating on your product");

        echo json_encode(array("statusCode"=>200));
    }

    public function timeRemaining(){
        if ($this->input->post('type') == 1){
                $expiryTime = $this->input->post('expiryTime');
                $product = $this->input->post('productID');

                $date = time("now");
                $newTime = strtotime($expiryTime);
                $diff = $expiryTime - $date;

                if($diff > 0){
    
                $day = floor($diff / (24 * 3600)); 
      
                $diff = ($diff % (24 * 3600)); 
                $hour = floor($diff / 3600); 
      
                $diff %= 3600; 
                $minutes = floor($diff / 60) ; 
      
                $diff %= 60; 
                $seconds = $diff; 

    
                echo json_encode(array("time"=>"$day days $hour hours $minutes minutes $seconds seconds","statusCode"=>200));
            }
            else{
               //Stops
            }
        }
        else{
            print_r('this_failed');     
        }
    }

    public function addFriend(){
        if (isset($_POST["addFriend"])){
            $currentUser = $this->input->post('userID');
            $friendUser = $this->input->post('friendID');

            if ($currentUser != $friendUser){
                $query = $this->item_model->getFriends($currentUser, $friendUser);
                if ($query == 0){
                    $this->item_model->addFriends($currentUser, $friendUser);
                    $this->notification_model->addNotification($friendUser, "Comment", "A Person has made you their friend!");
                    $this->session->set_flashdata('message', "You have now become Friends!");
                    $this->load->view('templates/addFriend');
                } else{
                    $this->session->set_flashdata('message', "You are already friends!");
                    $this->load->view('templates/addFriend');
                }
            }
            else{
                $this->session->set_flashdata('message', "You Can't Add Yourself As a Friend!");
                $this->load->view('templates/addFriend');
            }
        }
    }
}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('register_model');
        $this->load->helper('captcha');
        $this->load->config('email');
        $this->load->library('email');
    }

    public function index(){
        $values = array(
            'word' => '',   
            'word_length' => 8,  
            'img_path' => './captcha/',   
            'img_url' => base_url() .'captcha/', 
            'font_path' => base_url() . 'system/fonts/texb.ttf',
            'img_width' => '150',  
            'img_height' => 50,  
            'expiration' => 3600   
            );
        $data = create_captcha($values); 
        $_SESSION['captcha'] = $data['word'];
        $this->load->view('register', $data);
    }

    public function validation(){
        $this->form_validation->set_rules('username', 'User', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('repassword', '2Password', 'required');
        $this->form_validation->set_rules('captcha', 'captcha', 'required');
        if ($this->form_validation->run()){
            $checkUsername = $this->register_model->existingUsername($this->input->post('username'));
            $checkEmail = $this->register_model->existingEmail($this->input->post('email'));
            if ($checkUsername == 0){
                if ($checkEmail == 0){
                    $firstpassword = $this->input->post('password');
                    $secondpassword = $this->input->post('repassword');
                    $captcha = $this->input->post('captcha');
                    if ($firstpassword == $secondpassword){
                        $uppercaseValues = preg_match('@[A-Z]@' , $firstpassword);
                        $lowercaseValues = preg_match('@[a-z]@', $firstpassword);
                        $numberValues = preg_match('@[0-9]@', $firstpassword);
                        $specialCharactersValues = preg_match('@[^\w]@', $firstpassword);
                        if (!$uppercaseValues || !$lowercaseValues || !$numberValues || !$specialCharactersValues || strlen($firstpassword) < 8){
                            $this->session->set_flashdata('error', "You password is not strong enough, it should have at least 8 characters in length and should include at least one upper case letter, one number, and one special character.!");
                            $this->index();
                        } else{
                            $token = md5(rand());
                            $encrypt_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

                            if (is_array($_FILES)){
                                $fileName = $_FILES['picture']['name'];
                                $fileTmpName = $_FILES['picture']['tmp_name'];
                                $fileSize = $_FILES['picture']['size'];
                                $fileError = $_FILES['picture']['error'];
                                $fileType = $_FILES['picture']['type'];
                                $folderPath = "userAvatars/";
                                $properties = getimagesize($fileTmpName);
        
                                $fileExt = explode('.', $fileName);
                                $fileActualExt = strtolower(end($fileExt));
                                $fileNameNew = uniqid('', true);
                                $imageType = $properties[2];

                                switch($imageType){
                                    case IMAGETYPE_PNG:
                                        $imageID = imagecreatefrompng($fileTmpName);
                                        $layer = $this->imageResize($imageID, $properties[0], $properties[1]);
                                        imagepng($layer, $folderPath. $fileNameNew. ".". $fileActualExt);
                                    break;

                                    case IMAGETYPE_GIF:
                                        $imageID = imagecreatefromgif($fileTmpName);
                                        $layer = $this->imageResize($imageID, $properties[0], $properties[1]);
                                        imagegif($layer, $folderPath. $fileNameNew. ".". $fileActualExt);
                                    break;

                                    case IMAGETYPE_JPEG:
                                        $imageID = imagecreatefromjpeg($fileTmpName);
                                        $layer = $this->imageResize($imageID, $properties[0], $properties[1]);
                                        imagejpeg($layer, $folderPath. $fileNameNew. ".". $fileActualExt);
                                    break;

                                    default:
                                        $this->session->set_flashdata('error', "Invalid Image");
                                        exit;
                                        break;
                                }

                                $pictureUse = $folderPath. $fileNameNew. ".". $fileActualExt;
                            }
                            if ($captcha == $_SESSION["captcha"]){
                                $data = array(
                                    'username' => $this->input->post('username'),
                                    'password' => $encrypt_password,
                                    'email' => $this->input->post('email'),
                                    'token' => $token,
                                    'userAvatar' => $pictureUse
                                );

                                $query = $this->register_model->insert($data);

                                if ($query >0){
                                    $subject = "Verification of Email for Login";
                                    $message = "
                                    <p>Hi ".$this->input->post("username")."</p>
                                    <p>We are writing this email to verify your email with our MyBuy WebPage.</p>
                                    <p>First, please verify your email by clicking this <a href = '".base_url()."register/verification/".$token."'>link</a>.</p>
                                    <p>Once you click this, your email will be verified and you can login into MyBuy </p>
                                    <p>Kind Regards, </p>
                                    <p>The MyBuy Team </p>
                                    ";
                                
                                    $from = $this->config->item('smtp_user');
                                    $to = $this->input->post('email');
                                    $this->email->set_newline("\r\n");
                                    $this->email->from($from);
                                    $this->email->to($to);
                                    $this->email->subject($subject);
                                    $this->email->message($message);
                                    if ($this->email->send()){
                                        $this->session->set_flashdata('message', "Please check your email for a verification email");
                                        $this->index();
                                    }
                                    else{
                                    print_r("This failed");
                                    echo $this->email->print_debugger();
                                    }
                                }
                            }
                            else{
                                $this->session->set_flashdata('error', "You didn't get the Correct Captcha, try again!");
                                $this->index();
                            }
                        }
                    }
                    else{
                        $this->session->set_flashdata('error', "Your Password doesn't match");
                        $this->index();
                    }
                } else{
                    $this->session->set_flashdata('error', "This Email has been taken!");
                    $this->index();
                }
            } else{
                $this->session->set_flashdata('error', "This Username has been taken!");
                $this->index();
                print_r($checkUsername);
                print_r($checkEmail);
            }
        }
        else{
            print_r("Please Fill All The Details");
        }

    }

    public function verification(){
        if ($this->uri->segment(3)){
            $token = $this->uri->segment(3);

            if ($this->register_model->verify($token)){
                $data['success'] = '<h1 align = "center">Your Email has been successfully verified</h1>
                                    <p>You can now login in at <a href = "'.base_url().'login/index">here</a></p>';
            }
            else{
                $data['success']  = '<h1 align = "center">This link is invalid</h1>';
            }
        }
        $this->load->view('verify_email', $data);
    }

    public function captcha(){
        $image = imagecreatetruecolor(200,38);

        $background = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
        imagefill($image, 0, 0, $background);
        $linecolor = imagecolorallocate($image, 0xCC, 0xCC, 0xCC);
        $textcolor = imagecolorallocate($image, 0x33, 0x33, 0x33);

        for ($i = 0; $i < 5; $i++){
            imagesetthickness($image, rand(1,3));
            imageline($image, 0, rand(0,30), 200, rand(0,30), $linecolor);
        }

        $digit = '';
        for ($x = 15; $x <= 135; $x += 20){
            $digit .= ($num = rand(0,15));
            imagechar($image, rand(3,5), $x, rand(2,14), $num, $textcolor);
        }

        $_SESSION["captcha"] = $digit;

        header('Content-Type: image/png');
        imagepng($image);
        imagedestroy($image);
    }

    public function imageResize($image, $width, $height){
        $layer = imagecreatetruecolor(200, 200);
        imagecopyresampled($layer, $image, 0,0,0,0,200,200,$width, $height);

        return $layer;
    }
}

?>
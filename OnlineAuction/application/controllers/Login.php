<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('login_model');
        $this->load->model('chat_model');
        $this->load->library('email');
    }

    public function index(){
        $this->load->view('login');
    }

    public function validation(){
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            if (! empty($this->input->post('remember'))){
                if (!isset($_COOKIE["userPassword"])){
                    $cookie_expiration = time()+60*60*24*30;
                    setcookie("userName", $username, $cookie_expiration);
                    setcookie("userPassword", $password, $cookie_expiration);
                }
            } else{
                if (isset($_COOKIE["userName"])){
                    setcookie("userName", "");
                }
                if (isset($_COOKIE["userPassword"])){
                        setcookie("userPassword", "");
                }
            }
            $result = $this->login_model->validation($username, $password);
            if ($result == ''){
                $_SESSION["username"] = $username;
                $_SESSION["user_id"] = $this->session->userdata('id');
                $this->chat_model->setLoginEntrance($this->session->userdata('id'));
                $_SESSION["login_id"] = $this->session->userdata('login_id');
                redirect('papers');
            }
            else{
                $this->session->set_flashdata('message', $result);
                $this->load->view('login');
            }
        }
        else{
            $this->session->set_flashdata('message', 'Please Enter Valid Username and Password');
            $this->load->view('login');
        }
    }

    
    public function forgottenPassword(){
        $this->load->view('passwordrecovery');
    }

    public function forgottenPasswordVerification(){
        if(isset($_POST["submit-forgot-password"])){
            $selector = bin2hex(random_bytes(10));
            $token = random_bytes(32);
            $hashed_token = password_hash($token, PASSWORD_DEFAULT);

            $expires = date("U") + 1800;

            $email = $this->input->post('email');

            if ($this->login_model->pwdRowDelete($email) && $this->login_model->emailExists($email)){
                $data = array(
                    'passwordResetEmail' => $email,
                    'passwordResetSelector' => $selector,
                    'passwordResetToken' => $hashed_token,
                    'passwordResetExpiry' => $expires
                );

                $query = $this->login_model->insert($data);

                if ($query > 0){
                    $subject = "Forgotten Password Link";
                    $message = "
                    <p>Hi ".$this->input->post("username")."</p>
                    <p>We are writing this email to help you reset your Password with our MyBuy WebPage.</p>
                    <p>First, please change your email by clicking this <a href = '".base_url()."login/redirectResetPassword/".$selector."/". bin2hex($token)."'>link</a>.</p>
                    <p>Once you click this, follow the instructions and once done, you can login into MyBuy </p>
                    <p>Kind Regards, </p>
                    <p>The MyBuy Team </p>
                    ";

                    $from = $this->config->item('smtp_user');
                    $to = $this->input->post('email');
                    $this->email->set_newline("\r\n");
                    $this->email->from($from);
                    $this->email->to($to);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    if ($this->email->send()){
                        $this->session->set_flashdata('message', "Please check your email for a verification email");
                        $this->forgottenPassword();
                    }
                }   
            }
        }
    }

    public function redirectResetPassword(){
        if ($this->uri->segment(3) && $this->uri->segment(4)){
            $selector = $this->uri->segment(3);
            $token = $this->uri->segment(4);
        }

        if (ctype_xdigit($selector) != false && ctype_xdigit($token) != false){
            $data["selector"] = $selector;
            $data["validator"] = $token;
            $this->load->view('password-reset', $data);
        }
    }

    public function changePassword(){
        if (isset($_POST["reset-password-submit"])){
            $selector = $_POST["selector"];
            $validator = $_POST["validator"];
            $password = $_POST["password"];
            $repeatPassword = $_POST["redopassword"];
        }

        if ($password == $repeatPassword){
            $date = date("U");
            
            if ($this->login_model->findRow($selector, $date)){
                $row = $this->login_model->getRowToken($selector, $date);
                $token = hex2bin($validator);
                $tokenCheck = password_verify($token, $row);
                if ($tokenCheck === true){
                    $tokenEmail = $this->login_model->getRowEmail($selector, $date);
                    $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                    if($this->login_model->updatePwd($passwordHash, $tokenEmail)){
                        $this->session->set_flashdata('message', "Your Password has been successfully changed!");
                        $this->passwordSuccess();
                    }
                }
            }
        }
    }
    public function passwordSuccess(){
        $this->load->view('password-success');
    }   

    public function logout(){
        $userid = $this->session->userdata('id');
        $this->login_model->logout($userid);
        session_unset();
        $this->index();
    }
}

?>
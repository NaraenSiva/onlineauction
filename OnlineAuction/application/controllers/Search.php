<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('search_model');
    }

    public function search(){
        if (isset($_GET["search-submit"])){
            $searchEntry = $this->input->get('search');
            $searchCategory = $this->input->get('category');
            if ($searchCategory == 'All'){
                $searchCategory = null;
            }

            $query = $this->search_model->search($searchEntry, $searchCategory);

            if ($query->num_rows() > 0){
                $result_count = $query->num_rows();
            } else{
                $result_count = 0;
            }

            $data["searchEntry"] = $this->input->get('search');
            $data["username"] = $_SESSION["username"];
            $data["count"] = $result_count;
            $data["searchResults"] = $query;
            $data["title"] = "Search Results";
            setcookie('item_id', '', time()+5*60);

            $this->load->view('templates/header2', $data);
            $this->load->view('templates/search_result', $data);

            
        }
    }
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper('form');
        $this->load->model('chat_model');
        $this->load->helper('date');
    }

    public function index(){
        $userid = $_SESSION['user_id'];
        $query = $this->chat_model->findFriends($userid);


        $users = array();

        foreach($query->result_array() as $row){
            array_push($users, $row['friend_id']);
        }

        $friendsInfo = array();
        $friendOnlineStatus = array();

        foreach($users as $value){  
            array_push($friendsInfo, $this->chat_model->getInfo($value));
        }

        foreach($friendsInfo as $value){
            array_push($friendOnlineStatus, $this->chat_model->getOnlineStatus($value[0]['id'], $value[0]['username']));
        }

        $data['friendsInfo'] = $friendsInfo;
        $data['friendOnlineStatus'] = $friendOnlineStatus;
        $data["username"] = $_SESSION["username"];

        $this->load->view('templates/header3', $data);
        $this->load->view('chat-room', $data);
        $this->load->view('templates/footer');
    }

    public function messages(){
        if(isset($_GET["messages"])){
            $fromid = $_SESSION['user_id'];
            $toid = $this->input->get('id');

            $query = $this->chat_model->getChatHistory($fromid, $toid);

            $fromUserName = array();

            foreach($query as $value){
                array_push($fromUserName, $this->chat_model->getFromUser($value['from_user']));
            }

            $data["user_id"] = $this->input->get('id');
            $data["user_name"] = $this->input->get('username');
            $data["chatHistory"] = $query;
            $data["usernameHistory"] = $fromUserName;

           $this->load->view('templates/chatmessages', $data);
        }
    }

    public function sendMessage(){
        if ($this->input->post('type') == 2){
            $message = $this->input->post('message');
            $fromid = $this->input->post('fromid');
            $toid = $this->input->post('toid');
            $date = new \DateTime('now', new \DateTimeZone('Australia/Brisbane'));
            $datetimeFormat = 'Y-m-d H:i:s';
            $timestamp = $date->format($datetimeFormat);

            $this->chat_model->chatMessage($fromid, $toid, $message, $timestamp);
            echo json_encode(array("statusCode"=>200));
        }
    }

    public function reloadPage(){
        if ($this->input->post('type') == 1){
            $html = '';
            $to_user = $this->input->post('to_user');
            $to_username = $this->input->post('to_username');
            $from_user = $this->input->post('from_user');
            $query = $this->chat_model->getChatHistory($from_user, $to_user);
            $data["chatHistory"] = $query;
            $data["user_id"] = $to_user;
            $data["user_name"] = $to_username;
            $html = $this->load->view('templates/messages', $data, true);
            echo $html;
        }
    }
}
?>
<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <form action ="<?php echo site_url('register/validation');?>" method="post" enctype='multipart/form-data'>
            <div class = "register-form">
                <div class = "title">
                    <h2>Welcome to MyBuy!</h2>
                    <h4>Please Enter Your Details</h4>
                </div>
                <div id = "message">
                    <?php echo $this->session->flashdata('message');?>
                </div>
                <input type="text" placeholder="Enter Username" name="username" value = "<?php echo set_value('username')?>">
                <input type="text" placeholder="Enter your Email" name="email" value = "<?php echo set_value('email')?>">
                <input type="text" placeholder="Enter Password" name="password" value = "<?php echo set_value('password')?>">
                <input type="text" placeholder="Please Re-enter your Password" name="repassword" value = "<?php echo set_value('repassword')?>">
                <div class = "input-group">
                    <input type = "text" placeholder = "Type in values you see below" name = "captcha" value = "<?php echo set_value('captcha')?>">
                    <div class="captcha"><?php echo $image;?></div>
                </div>
                <div id = "avatar">
                    <label for = "types">Upload Image of Yourself:</label>
                    <input type = "file" name = "picture" id = "photo">
                </div>
                <div id = "error-message">
                    <?php echo $this->session->flashdata('error');?>
                </div>
                <button type="submit">Register</button>
            </div>
        </form>   
    </body>
</html>
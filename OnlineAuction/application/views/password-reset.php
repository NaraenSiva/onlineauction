<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
    </head>
    <body>
        <form action ="<?php echo site_url('login/changePassword');?>" method="post">
            <div class = "register-form">
                <div class = "title">
                    <h2>Password Reset Page</h2>
                    <h4>Please Follow the Instructions to Reset password</h4>
                </div>
                <div class = "resetpassword">
                    <input type = "hidden" name = "selector" value = "<?php echo $selector; ?>">
                    <input type = "hidden" name = "validator" value = "<?php echo $validator ?>">
                    <input type="text" placeholder="Enter New Password" name="password" required>
                    <input type = "text" placeholder="Re-enter this New Password" name = "redopassword" required>
                    <button type="submit" name = "reset-password-submit">Reset Password</button>
                </div>
            </div>
        </form>   
    </body>
</html>
<html>
    <head>
        <title>Chat Room</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <table class = "table table-bordered table-striped">
            <tr>
                <th>Username</th>
                <th>Status</th>
                <th>Chat</th>
            </tr>
                <?php foreach($friendOnlineStatus as $value):?>
                    <tr>
                        <td>
                            <?php echo $value['username'];?>
                        </td>
                        <td>
                            <?php if ($value['status'] == 'True'):?>
                                <span class = "label label-success">Online</span>
                            <?php else :?>
                                <span class = "label label-danger">Offline</span>
                            <?php endif; ?>
                        </td>
                        <td>
                            <form action = "<?php echo site_url('chat/messages');?>" method = "get">
                                <input type = "hidden" name = "id" value = "<?php echo $value['id'];?>">
                                <input type = "hidden" name = "username" value = "<?php echo $value['username'];?>">
                                <button type = "submit" class = "btn btn-info btn-xs start_chat" name = "messages" value = "1">Start Chat</button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tr>
        </table>
    </body>
</html>



            
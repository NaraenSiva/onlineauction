<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
    </head>
    <body>
        <form action ="<?php echo site_url('login/forgottenPasswordVerification');?>" method="post">
            <div class = "register-form">
                <div class = "title">
                    <h2>Forgotten your Password?</h2>
                    <h4>Please Enter the Email registered to your account</h4>
                </div>
                <div id = "message">
                    <?php echo $this->session->flashdata('message');?>
                </div>
                <div class = "resetpassword">
                    <input type="text" placeholder="Enter Email" name="email" required>
                    <button type="submit" name = "submit-forgot-password">Reset Password</button>
                </div>
            </div>
        </form>   
    </body>
</html>
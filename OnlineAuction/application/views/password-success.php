<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
    </head>
    <body>
        <form action ="<?php echo site_url('login/index');?>" method="post">
            <div class = "register-form">
                <div class = "title">
                    <h2>Password Reset Page</h2>
                </div>
                <div id = "message">
                    <?php echo $this->session->flashdata('message');?>
                </div>
                    <button type="submit" name = "reset-password-submit">Return to Login Page</button>
                </div>
            </div>
        </form>   
    </body>
</html>
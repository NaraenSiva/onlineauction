<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    </head>
    <body>
        <form action ="<?php echo site_url('item/leaveComment');?>" method="post">
            <div class = "register-form">
                <div class = "title">
                    <h2>Welcome to MyBuy Comments</h2>
                    <h4>Please leave a comment on this particular Item</h4>
                </div>
                <textarea id = "comment" placeholder="Leave a Comment Here" name="comment_message"></textarea>
                <input type = "hidden" name = "item_id" value = <?php echo $_COOKIE["item_id"];?>>
                <button type="submit" name = "comment">Submit</button>
            </div>
        </form>   
    </body>
</html>
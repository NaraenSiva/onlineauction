<html>
    <head>
        <title>Product Upload to MyBuy</title>
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style6.css">
    </head>
    <body>
        <div id = "product-upload">
            <form action = "<?php echo site_url('item/uploadProduct');?>" method = "post" enctype='multipart/form-data'> 
                <div class = "title">
                    <h2>Want to Sell a Product?</h2>
                    <h4>Please fill in these details!</h4>
                </div>
                <div id = "message">
                    <?php echo $this->session->flashdata('message');?>
                </div>
                
                <label for = "name">Enter Product Name:</label>
                <input type = "text" placeholder = "Enter Product Name" name = "productname" value = "<?php echo set_value('productname')?>">
                
                <label for = "types">Choose a Product Category:</label>
                <select id = "types" name = "producttype"value = "<?php echo set_value('producttype')?>">
                    <option value = "Home&Garden">Home & Garden</option>
                    <option value = "Electronics">Electronics</option>
                    <option value = "Fashion">Fashion</option>
                    <option value = "Furniture">Furniture</option>
                    <option value = "Sports">Sports</option>
                
                </select>
                <label for = "types">Choose a Product Condition:</label>
                <select id = "types" name = "productcondition"value = "<?php echo set_value('productcondition')?>">
                    <option value = "BrandNew">Brand New</option>
                    <option value = "Used">Used</option>
                </select>
                <label for = "types">List Product Starting Price (Aus Dollars):</label>
                <input type = "text" placeholder = "Enter Product Starting Price" name = "productprice"value = "<?php echo set_value('productprice')?>">
                
                <label for = "types">Choose a Length of Time to List Product (in Days):</label>
                <input type = "text" placeholder = "Enter Number of Days on Marketplace" name = "productexpiry" value = "<?php echo set_value('productexpiry')?>">
                
                <label for = "types">Upload Image of Product:</label>
                <input type = "file" name = "picture" id = "photo">
                
                <button type = "submit" name = "submit-product-items">Upload Product</button>
            </form>
    </body>
</html>
<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style4.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style2.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel = "stylesheet" href = "https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    </head>
    <body>
        <div id = "bidding">
            <div id = "auction-bid">
                <div id = "image">
                    <?php $image = $productImage;?>
                    <img id = "item" src = "<?php echo base_url($image); ?>"/>
                </div>
                <div id = "bid">
                </div>
                <form action="<?php echo site_url('item/bid');?>" method="post">
                    <div id = "bid-area">
                        <input type = "hidden" id = "productID" value = <?php echo $productId;?>>
                        <input type = "hidden" id = "userID" value = "<?php echo $_SESSION["user_id"];?>">
                        <input id = "bid-text" type="text" placeholder="Enter Bid Price (Number Only)" required>
                        <button id = "bid-button" type="submit" name = "bid">Bid Now</button>
                    </div>
                </form>
            </div>
            <div id = "item-information">
                <div id = "item-title">
                    <h3><?php echo $productName;?></h3>
                </div>
                <div id = "item-seller">
                    <p>By <a href = "<?php echo base_url('item/seller'); ?>"> <?php echo $productOwner;?></a></p>
                </div>
                <div id = "item-condition">
                    <p>Condition: <?php echo $productCondition; ?></p>
                </div>
                <div id = "item-expiry">
                    <input type = "hidden" id = "expiry-time" value = "<?php echo $currentTime;?>">
                    <p>Time Remaining:</p>
                    <p id = "time-remaining"></p>
                </div>
                <div id = "item-startingprice">
                    <p>Starting Price: $ <?php echo $productPrice;?></p>
                </div>
                <div id = "additional-features">
                    <form action = "<?php echo base_url('item/watchlist'); ?>" method = "POST">
                        <input type = "hidden" name = "productID" id = "productID" value = "<?php echo $productId;?>">
                        <button type = "submit" name = "watchlist" id = "watchlist">Add to Watchlist</button>
                    </form>
                    <form action = "<?php echo base_url('item/addFriend'); ?>" method = "POST">
                        <input type = "hidden" name = "userID" value = "<?php echo $_SESSION["user_id"];?>">
                        <input type = "hidden" id = "toid" name = "friendID" value = "<?php echo $productOwnerID;?>">
                        <button type = "submit" name = "addFriend" id = "addFriend">Add Seller as Friend</button>
                    </form>
                    <form action = "<?php echo base_url('item/comment'); ?>">
                        <button type = "submit" id = "comment">Leave a Comment</button>
                    </form>
                    <div id = "Review">
                        <div id = "stars">
                            <p>Review this Product!</p>
                            <div padding = "20px">
                                <i class = "fa fa-star fa-2x" data-index = "0"></i>
                                <i class = "fa fa-star fa-2x" data-index = "1"></i>
                                <i class = "fa fa-star fa-2x" data-index = "2"></i>
                                <i class = "fa fa-star fa-2x" data-index = "3"></i>
                                <i class = "fa fa-star fa-2x" data-index = "4"></i>
                            </div>
                        </div>   
                    </div>
                    <div id = "success">
                        <p>Thank you for leaving a review of this product!</p>
                    </div>
                </div>
            </div>
        </div>
        <hr class = "breaker">
    </body>
    <script>
        var ratingValue = -1;

        $(document).ready(function(){
            setInterval(() => {
                timerCountDown();
            }, 1000);

            $("#success").hide();
            $('#expiryButton').hide();
            resetColours();


            $('.fa-star').on('click', function(){
                ratingValue = parseInt($(this).data('index'));
                sendRating();
            });

            $('.fa-star').mouseover(function(){
                resetColours();

                var currentIndex = parseInt($(this).data('index'));

                for (var i = 0; i <= currentIndex; i++){
                    $('.fa-star:eq('+i+')').css('color', 'green');
                };
            });

            $('.fa-star').mouseleave(function(){
                resetColours();

                if (ratingValue != -1){
                    for (var x = 0; x <= ratingValue; x++){
                        $('.fa-star:eq('+x+')').css('color', 'green');
                    };
                };
            });
        });

        function resetColours(){
            $('.fa-star').css('color', 'black');
        }

        function sendRating(){
            var productId = $('#productID').val();
            var userId = $('#userID').val();
            var productOwner = $("#toid").val();
            $.ajax({
                type: "post",
                url: "<?php echo base_url("Item/saveRatings");?>",
                data: {
                    type: 1,
                    userId: userId,
                    productOwner: productOwner,
                    productId: productId,
                    ratingValue: ratingValue
                },
                cache: false,
                success: function(dataResult){
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode==200){
                        $("#Review").hide();
                        $("#success").show();
                    }
                }
            });
        }

        function timerCountDown(){
            var time = $("#expiry-time").val();
            var productID = $('#productID').val();
            $.ajax({
                type: "post",
                url: "<?php echo base_url("Item/timeRemaining");?>",
                data: {
                    type: 1,
                    expiryTime: time,
                    productID: productID
                },
                cache: false,
                success: function(data){
                    if (!data){
                        $('#time-remaining').html("The Item has expired, No Further Bids can be made");
                        if ($('#time-remaining').html() == "The Item has expired, No Further Bids can be made"){
                            $('#bid-text').attr("disabled", "disabled");
                            $('#bid-button').attr("disabled", "disabled");
                            $('#watchlist').attr("disabled", "disabled");
                            $('#comment').attr("disabled", "disabled");
                            $('#stars').hide();
                            $("#expiryButton").show();
                            var toid = $("#toid").val();
                        }
                    } 
                    else{
                        var dataResult = JSON.parse(data);
                        $('#time-remaining').html(dataResult.time);
                    }   
                }
            });
        }

         $(document).ready(function(){
            update_bid_history();
        });

        function update_bid_history(){
            var productId = $("#productID").val();
            $.ajax({
                type: "post",
                url: "<?php echo base_url("Item/reloadBid");?>",
                data: {
                    type: 1,
                    productId : productId
                },
                cache: false,
                success: function(html){
                    $('#bid').html(html);
                }   
            });
        };
        
        $(document).ready(function() {
            $('#bid-button').on('click', function() {
                var bidvalue = $('#bid-text').val();
                var productID = $('#productID').val();
                var userID = $('#userID').val();
                $("#bid-button").attr("disabled", "disabled");
                $.ajax({
                    url: "<?php echo base_url("Item/bid");?>",
                    type: "POST",
                    data: {
                        type: 2,
                        bidvalue : bidvalue,
                        productID : productID,
                        userID : userID
                    },
                    cache: false,
                    success: function(dataResult){
                        var dataResult = JSON.parse(dataResult);
                        if(dataResult.statusCode==200){
                            $("#bid-button").removeAttr("disabled");
                            $("#bid-text").val('').change();
                            update_bid_history();
                        }
                    }
                });
            });
        });    
    </script>
<html>

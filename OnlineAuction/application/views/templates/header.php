<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
    </head>
    <body>
        <div id = "heading">
            <div id = "spread">
                <div id = "nav">
                    <ul>
                        <div id = "nav-import">
                            <a>Welcome Back, <?php echo $username;?>!</a>
                        </div>
                        <div id = "nav-import" class = "noti">  
                            <img src = "<?php echo base_url('images/bell-solid.svg');?>"/>
                            <div class = "dropdown">
                                <button id = "dropbtnnoti" class = "dropbtnnoti" onclick = "openTab()">Notifications</button>
                                <div id = "content" class = "contentnoti"></div>
                                <span id = "badge" class = "badge"></span></a>
                            </div>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/user-solid.svg'); ?>"/>
                            <div class = "dropdown">
                                <button class = "dropbtn">MyProfile</button>
                                <div class = "content">
                                    <a href = "<?php echo base_url('papers/index'); ?>">Home</a>
                                    <a href = "<?php echo base_url('papers/getDetails'); ?>">My Details</a>
                                    <a href = "#">My Purchases</a>
                                    <a href = "<?php echo base_url('papers/getItems');?>">My Items</a>
                                    <a href = "<?php echo base_url('item/sellItem'); ?>">Sell Item</a>
                                    <a href = "<?php echo base_url('chat/index'); ?>">Messages</a>
                                </div>
                            </div>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/shopping-cart-solid.svg'); ?>"/>
                            <a href = "<?php echo base_url('papers/getWatchList');?>">MyWatchList</a>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/sign-out-alt-solid.svg'); ?>"/>
                            <a href = "<?php echo base_url("login/logout");?>">Logout</a>
                        </div>
                    </ul>
                </div>
            </div>
            <div id = "title">
                <h1><a href = "<?php echo base_url('papers/index'); ?>">MyBuy</a></h1>
            </div>
            <div class = "wrapper">
                <form action = "<?php echo site_url('search/search');?>" method = "GET">
                    <div class = "search_bar">
                        <div class = "search_field">
                            <input id = "search" type = "text" class= "input" placeholder = "Search" name = "search" autocomplete = "off" required>
                            <div id = "result"></div>
                        </div>
                        <div class = "dropdown">
                            <select id = "category" name = "category">
                                <option value ="All">All Categories</option>
                                <option value = "Furniture">Furniture</option>
                                <option value = "Home&Garden">Home Appliances</option>
                                <option value = "Fashion">Fashion</option>
                                <option value = "Electronics">Electronics</option>
                            </select>
                        </div>
                        <div class = "button">
                            <input type = "hidden" name = "search-submit" value = "1">
                            <input type="submit" class="nav-submit-input" value="Search"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script>
        $(document).ready(function(){
            $("#search").keyup(function(){
                var query = $("#search").val();
                
                if (query.length > 0){
                    $.ajax({
                        url: "<?php echo base_url("papers/search");?>",
                        method: "POST",
                        data: {
                            search: 1,
                            value: query
                        },
                        success: function(data){
                            $("#result").html(data);
                        },
                        dataType: 'text'
                    });
                }
            });

            $(document).on('click', '#notifiy', function(){
                var product = $(this).text();
                $("#search").val(product);
                $("#result").html("");
            });
        });
    </script> 
    </body>
    <script>
        $(document).ready(function(){
            function load_notifications(notifications = ''){
                $.ajax({
                    url: "<?php echo base_url("Notifications/updateNotification");?>",
                    method: "POST",
                    data: {
                        notifications : notifications
                    },
                    dataType: "json",
                    success: function(data){
                        $('.contentnoti').html(data.notifications);
                        $('.badge').html(data.unseen_notifications);
                    }
                });
            }
            load_notifications();

            $(document).on('click', '.dropbtnnoti', function(){
                $('.badge').html();
                setTimeout(() => {
                    load_notifications('yes');  
                }, 5000);
            });


            setInterval(function(){
                load_notifications();
            }, 1000);
        });
    </script>
    <script>
        function openTab(){
            document.getElementById("content").classList.toggle("show");
        }

        window.onclick = function(event){
            if(!event.target.matches('.dropbtnnoti')){
                var dropdown = document.getElementsByClassName("contentnoti");
                var i;
                for (i = 0; i < dropdown.length; i++){
                    var openDropdown = dropdown[i];
                    if (openDropdown.classList.contains('show')) {
                        openDropdown.classList.remove('show');
                    }
                }
            }
        }
    </script>
</html>
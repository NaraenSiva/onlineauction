<html>
    <head>
        <title>Chat Box</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style7.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class = "user_dialog" id = "<?php echo $user_id;?>">
        <div id = "title">
            <h2>You are chatting to <?php echo $user_name;?></h2>
        </div>
        <div class = "chat_history" id = "history">
        </div>
        <form action = "<?php echo site_url('chat/sendMessage');?>" method = "post">
            <div class = "form-group">
                <textarea name = "chat_message" id = "message" class = "form-control"></textarea>
            </div>
            <input type = "hidden" name = "from_id" id = "from_id" value = "<?php echo $_SESSION['user_id'];?>">
            <input type = "hidden" name = "to_id" id = "to_id" value = "<?php echo $user_id;?>">
            <input type = "hidden" name = "to_username" id = "to_username" value = "<?php echo $user_name;?>">
            <div class = "form_group">
                <button type = "submit" name = "send_chat" id = "chat_send" class = "btn btn-info send_chat">Send</button>
            </div>
        </form>
    </div>
    </body>
    <script>
        $(document).ready(function(){
            setInterval(() => {
                update_chat_history();
            }, 1500);
        });

        function update_chat_history(){
            var to_username = $('#to_username').val();
            var to_user = $('#to_id').val();
            var from_user = $('#from_id').val();
            $.ajax({
                type: "post",
                url: "<?php echo base_url("Chat/reloadPage");?>",
                data: {
                    type: 1,
                    to_user: to_user,
                    to_username: to_username,
                    from_user: from_user
                },
                cache: false,
                success: function(html){
                    $('#history').html(html);
                }   
            });
        };

        $(document).ready(function() {
            $('#chat_send').on('click', function() {
                var message = $('textarea#message').val();
                var fromid = $('#from_id').val();
                var toid = $('#to_id').val();
                $("#chat_send").attr("disabled", "disabled");
                $.ajax({
                    url: "<?php echo base_url("Chat/sendMessage");?>",
                    type: "POST",
                    data: {
                        type: 2,
                        message: message,
                        fromid: fromid,
                        toid: toid
                    },
                    cache: false,
                    success: function(dataResult){
                        var dataResult = JSON.parse(dataResult);
                        if(dataResult.statusCode==200){
                            $("#chat_send").removeAttr("disabled");
                            $("textarea#message").val('').change();
                            update_chat_history();
                        }
                    }
                });
            });
        });
    </script>
</html>
<html>
    <head>
        <title>UserProfile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id = "Items">
            <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	        </div>
            <table class = "table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Time Until Expiry</th>
                        <th>Remove Item</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($times as $time):?>
                        <tr>
                            <td>
                                <?php echo $time[1];?>
                            </td>
                            <td>
                                <?php echo $time[2];?>
                            </td>
                            <td>
                                <?php echo $time[3];?>
                            </td>
                            <td>
                                <?php if ($time[3] == "Expired"):?>
                                    <button id = "remove" type = "submit" class = "btn btn-primary" name = "removeItem" value = "<?php echo $time[0];?>">Remove Item from MyBuy</button>
                                <?php endif;?>
                                <?php $_COOKIE["productID"] = $time[0];?>
                            </td>
                        <tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </body>
    <script>
        $(document).ready(function(){
            $('#remove').on('click', function() {
                var productID = $('#remove').val();
                $("#remove").attr("disabled", "disabled");
                removeBidItem(8, productID);
            });
        });

        function removeBidItem(type, productID){
            $.ajax({
                    url: "<?php echo base_url("Papers/removeItem");?>",
                    type: "POST",
                    data: {
                        type: type,
                        productID: productID
                    },
                    cache: false,
                    success: function(dataResult){
                        var dataResult = JSON.parse(dataResult);
                        if (dataResult.statusCode==200){
                            $("#remove").removeAttr("disabled");
                            $("#success").show();
                            $('#success').html('This Item has been removed, refresh your browser to see the changes'); 						
                        }
                        else if(dataResult.statusCode==201){
                            alert("Error occured !");   
                        }
                    }  
                });
            }
        
    </script>
</html>
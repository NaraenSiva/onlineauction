<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style1.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    </head>
    <body>
        <div id = "navbar">
            <ul>
                <li>
                    <a href = '<?php echo base_url('login/index'); ?>'>Home</a>
                </li>   
                <li>
                    <a href = "#">Fashion</a>
                </li>
                <li>
                    <a href = "#">Electronics</a>
                </li>
                <li>
                    <a href = "#">Sports</a>
                </li>
                <li>
                    <a href = "#">For the Kids</a>
                </li>
                <li>
                    <a href = "#">Home and Garden</a>
                </li>
            </ul>
        </div>

        <div id = "Item-Information">
            <div id = "Top-Hits">
                <div id = "header">
                    <h3>Here are some of the Top Hits for Today!</h3>
                </div>
                <div id = "items">
                    <a class = "click" href = "<?php echo base_url('item/index'); ?>">
                        <div id = "item-1">
                            <div id = "image">
                                <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                            </div>
                            <div id = "item-name">
                                <p>4 Man Couch</p>
                            </div>
                            <div id = "item-price">
                                <p> $45</p>
                            </div>
                        </div>
                    </a>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id = "Recommended-Items">
                <div id = "header">
                    <h3>Here are some Recommended Hits for Today!</h3>
                </div>
                <div id = "items">
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                    <div id = "item-1">
                        <div id = "image">
                            <img src = "<?php echo base_url('images/furniture.jpeg'); ?>"/>
                        </div>
                        <div id = "item-name">
                            <p>4 Man Couch</p>
                        </div>
                        <div id = "item-price">
                            <p> $45</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class = "breaker">
    </body>
    <script>
        $(document).ready(function(){
            setInterval(() => {
            getNotifications();
        }, 3000);

        function getNotifications(view = ''){
            $.ajax({
                type: "post",
                url: "<?php echo base_url("papers/checkNotifications");?>",
                data: {
                    view: view
                }
            });
        };
    });
    </script>
</html>
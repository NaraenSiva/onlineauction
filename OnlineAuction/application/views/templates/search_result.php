<html>
    <head>
        <title>Search Results</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style8.css">
    </head>
    <body>
        <div id = "title">
            <h2><?php echo $count;?> Results for <?php echo $searchEntry;?></h2>
        </div>
        <hr>
        <div id = "results">
            <?php if ($count == 0):?>
                <p id = "matches">Sorry, no matches found for your search term</p>
            <?php else :?>
                <?php foreach($searchResults->result() as $row):?>
                    <div id = "result">
                        <div id = "box">
                            <div id = "image">
                                <?php $image = $row->productImage; ?>
                                <img src = "<?php echo base_url($image); ?>"/>
                            </div>
                            <div id = "details">
                                <div id = "name">
                                    <h2><?php echo $row->productName;?></h2>
                                </div>
                                <div id = "owner">
                                    <p>By <?php echo $row->productOwner;?></p>
                                </div>
                                <div id = "condition">
                                    <p>Condition: <?php echo $row->productCondition;?></p>
                                </div>
                                <div id = "price">
                                    <p>AU $<?php echo $row->productPrice;?></p>
                                </div>
                                <form action = "<?php echo site_url('item/index');?>" method="get">
                                    <div id = "Bid">
                                        <input type = "hidden" name = "id" value = "<?php echo $row->id;?>">
                                        <button type = "submit" name = "item" value = "1">Bid on this</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    <div>
                <?php endforeach;?>
            <?php endif;?>
        </div>
    </body>
</html>
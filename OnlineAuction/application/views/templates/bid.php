<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style4.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style2.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel = "stylesheet" href = "https://use.fontawesome.com/releases/v5.8.1/css/all.css">                    
    </head>            
    <body>
        <?php foreach($bids as $bid):?>
            <div id = "message">
                <p id = "bid-value">
                    User bid $<?php echo $bid->bidAmount;?>
                </p>
                <p id = "time">
                    <?php echo $bid->timestamp;?>
                </p>
            </div>
        <?php endforeach;?>
    </body>
</html>
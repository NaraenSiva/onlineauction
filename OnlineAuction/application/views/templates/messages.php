<html>
    <head>
        <title>Chat Box</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style7.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php for($x = 0; $x < sizeof($chatHistory); $x++):?>
            <p id = "name">
                <?php if($chatHistory[$x]['from_user'] == $user_id): ?>
                    <?php echo $user_name;?>
                <?php else : ?>
                    <?php echo $_SESSION["username"];?>
                <?php endif; ?>
            </p>
            <p id = "message">
                <?php echo $chatHistory[$x]['message'];?>
                <em><?php echo $chatHistory[$x]['timestamp'];?></em>
            </p>
        <?php endfor;?>
    </body>
</html>
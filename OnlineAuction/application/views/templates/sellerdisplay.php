<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style5.css">
        <link rel="stylesheet" href = "<?php echo base_url(); ?>css/fonts/css/fontawesome.css">
    </head>
    <body>
        <div id = "seller">
            <div class = "vertical">
                <a href = "#" class = "active">Seller Home</a>
                <a href = "#">Recent Sales</a>
                <a href = "#">Message Seller</a>
                <a href = "#">Reviews</a>
                <a href = "#">Similar Sellers</a>
            </div>
            <div id = "seller-information">
                <div id = "seller-name">
                    <h3>Herald9382</h3>
                </div>
                <div id = "seller-location">
                    <h4>Location: Gold Coast</h4>
                </div>
                <div id = "seller-rating">
                </div>
                <div id = "seller-sales-most">
                    <h4>Most Sales by Category: Furniture</h4>
                </div>
                <div id = "seller-buys-most">
                    <h4>Most Buys by Category: Electronics</h4>
                </div>
                <div id = "seller-active-status">
                    <h4>Active 1 hour ago</h4>
                </div>
                <div id = "seller-friend-request">
                    <form action = "<?php echo base_url('seller/friendrequest'); ?>">
                        <button type = "submit">Add to FriendList</button>
                    </form>
                </div>
                <div id = "seller-leave-review">
                    <form action = "<?php echo base_url('seller/review'); ?>">
                        <button type = "submit">Add a Review on Seller</button>
                    </form>
                </div>
            </div>
            <div id = "seller-frame">
                <div id = "seller-image">
                    <img id = "image" src = "<?php echo base_url('images/person.jpg'); ?>"/>
                </div>
            </div>
        </div>
    </body>
</html>
<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style.css">
        <link rel="stylesheet" href = "<?php echo base_url(); ?>css/fonts/css/fontawesome.css">
    </head>
    <body>
    <div id = "heading">
            <div id = "spread">
                <div id = "nav">
                    <ul>
                        <div id = "nav-import">
                            <a>Welcome Back, <?php echo $username;?>!</a>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/bell-solid.svg'); ?>"/>
                            <a href = "#">Notifications</a>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/user-solid.svg'); ?>"/>
                            <div class = "dropdown">
                                <button class = "dropbtn">MyProfile</button>
                                <div class = "content">
                                    <a href = "#">Home</a>
                                    <a href = "#">My Purchases</a>
                                    <a href = "<?php echo base_url('item/sellItem'); ?>">Sell Item</a>
                                </div>
                            </div>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/shopping-cart-solid.svg'); ?>"/>
                            <a href = "#">MyWatchList</a>
                        </div>
                        <div id = "nav-import">
                            <img src = "<?php echo base_url('images/sign-out-alt-solid.svg'); ?>"/>
                            <a href = "<?php echo base_url("login/logout");?>">Logout</a>
                        </div>
                    </ul>
                </div>
            </div>
            <div id = "title">
                <h1>Seller Info: Herald9382</h1>
            </div>
        </div>
    </body>
</html>

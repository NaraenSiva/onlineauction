<html>
    <head>
        <title>UserProfile</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div id = "watchlist">
            <div class="alert alert-success alert-dismissible" id="success" style="display:none;">
	            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
	        </div>
            <table class = "table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Time Until Expiry</th>
                        <th>Remove Item</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($products) == 0):?>
                        <h2 align = "center">No Items In WatchList</h2>
                    <?php else:?>
                        <?php foreach($products as $product):?>
                            <tr>
                                <?php for($x = 0; $x <= sizeof($products)-1; $x++):?>
                                    <td>
                                        <?php echo $product[$x]->productName;?>
                                    </td>
                                    <td>
                                        <?php echo $product[$x]->productPrice;?>
                                    </td>
                                    <td>
                                        <?php echo $product[$x]->productExpiry;?>
                                    </td>
                                    <td>
                                        <button id = "remove" type = "submit" class = "btn btn-primary" name = "messages" value = "<?php echo $product[$x]->id;?>">Remove Item from WatchList</button>
                                    </td>
                                <?php endfor;?>
                            <tr>
                        <?php endforeach; ?>
                    <?php endif;?>
                </tbody>
            </table>
        </div>
    </body>
    <script>
        $(document).ready(function() {
            $('#remove').on('click', function() {
                var productID = $('#remove').val();
                $("#remove").attr("disabled", "disabled");
                $.ajax({
                    url: "<?php echo base_url("Papers/removeWatchList");?>",
                    type: "POST",
                    data: {
                        type: 1,
                        productID: productID
                    },
                    cache: false,
                    success: function(dataResult){
                        var dataResult = JSON.parse(dataResult);
                        if(dataResult.statusCode==200){
                            $("#remove").removeAttr("disabled");
                            $("#success").show();
                            $('#success').html('This Item has been removed Refresh your browser to see the changes'); 						
                        }
                        else if(dataResult.statusCode==201){
                            alert("Error occured !");
                        }  
                    }
                });
            });
        });
    </script>
</html>

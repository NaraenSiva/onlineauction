<html>
    <head>
        <title>Web Information Systems Project </title>
        <meta charset = "UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>/css/bootstrap/css/bootstrap.css">
        <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>/css/style3.css">
    </head>
    <body>
        <form action="<?php echo site_url('login/validation');?>" method="post">
            <div class="login-form">
                <div class = "title">
                    <h2>Welcome to MyBuy!</h2>
                </div>
                <div id = "message">
                    <p><?php echo $this->session->flashdata('message');?></p>
                </div>
                <label for="username"><b>Username</b></label>
                <input type="text" placeholder="Enter Username" name="username" value = "<?php if (isset($_COOKIE["userName"])){ echo $_COOKIE["userName"];}?>" required>

                <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password" value = "<?php if (isset($_COOKIE["userPassword"])){ echo $_COOKIE["userPassword"];}?>" required>

                <div id = "remember">
                    <label for="remember-me"><b>Remember Me</b></label>
                    <input type ="checkbox" name = "remember" <?php if(isset($_COOKIE["userName"])){ ?> checked <?php } ?>/>  
                </div>
                
                <button type="submit">Login</button>
            </div>

            <div class="helper" style="background-color:#f1f1f1">
                <span class = "register">Haven't Registered Yet? <a href = "<?php echo base_url("register/index");?>">Click Here</a></span>
                <span class="psw">Forgot <a href="<?php echo base_url("login/forgottenPassword");?>">password?</a></span>
            </div>
        </form>
    </body>
</html>
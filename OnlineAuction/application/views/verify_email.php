<!DOCTYPE html>
<html>
<head>
 <title>Complete Login Register system in Codeigniter</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
 <link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/style3.css">
</head>

<body>
 <div class="container">
        <form action ="<?php echo site_url('login/index');?>" method="post" >
            <div class = "register-form">
                <div class = "title">
                    <h2>MyBuy Email Verification</h2>
                </div>
                <div id = "message">
                    <?php echo $success;?>
                </div>
                <div class = "resetpassword">
                    <button type="submit" name = "submit-forgot-password">Return to Login Page</button>
                </div>
            </div>
        </form>   
    </body>
 </div>
</body>
</html>

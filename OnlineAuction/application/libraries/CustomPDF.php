<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'third_party/fpdf/fpdf.php');

class CustomPDF extends FPDF {
    public function getPDFInvoice($products, $bidders, $sellers, $highestBidder){
        $pdf = new fpdf();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(60,20, "MyBuy Invoice");
        $pdf->Cell(60);
        foreach($products as $row){
            $pdf->Cell(60,20, "Invoice for Order: ".$row->id);
            $pdf->Ln();
            $pdf->setFont('helvetica', 'I', 10);
            $pdf->Cell(190, 7, 'Order Details', 1, 2);
            $pdf->setFont('helvetica', '', 10);
            $y = $pdf->getY();
            $pdf->MultiCell(95, 8, "Order Id: ".$row->id."\nOrderStatus : Pending", 1);
            $x = $pdf->GetX();
            $pdf->setXY($x+95,$y);
            $pdf->MultiCell(95, 8, "Payment Method: Private Payment\nDate Issued: 25-09-18",1);
        }
        $pdf->setFont('helvetica', 'I', 10);
        $pdf->setXY($x, $y +25);
        $pdf->Cell(95, 7, 'Account Details', 1,2);
        $pdf->setFont('helvetica', '', 10);
        foreach($bidders as $bidder){
            $pdf->MultiCell(95, 8, "Account Name: ".$bidder->username."\nEmail Address : ".$bidder->email, 1);
        }
        $pdf->setFont('helvetica', 'I', 10);
        $pdf->setXY($x+95, $y+25);
        $pdf->Cell(95, 7, 'Seller Details', 1,2);
        $pdf->setFont('helvetica', '', 10);
        foreach($sellers as $seller){
            $pdf->MultiCell(95, 8, "Account Name: ".$seller->username."\nEmail Address : ".$seller->email, 1);
        }

        $pdf->setXY($x+5, $y + 60);
        $pdf->setFont('helvetica', 'B', 10);
        $pdf->Cell(180, 7, 'Items on Invoice', 1, 2);
        $pdf->SetFillColor(255,255,255);
        $pdf->Cell(60,10, "Name of Item", 1, 0, 'S', true);
        $pdf->Cell(60,10, "Department of Item", 1, 0, 'S', true);
        $pdf->Cell(60,10, "Total Price", 1, 0, 'S', true);
        $pdf->setFont('helvetica', 'I', 10);
        $y = $pdf->getY();
        $pdf->setXY($x+5, $y+10);
        foreach($products as $product){
            $pdf->Cell(60,10, $product->productName, 1, 0, 'C',1);
            $pdf->Cell(60,10, $product->productType, 1, 0, 'C', true);
        }
        foreach($highestBidder as $high){
            $pdf->Cell(60,10, "$".$high->bidAmount, 1, 0, 'C', true);   
        }
        $y = $pdf->getY();
        $pdf->setXY($x, $y+ 20);
        $pdf->Cell(190,10, "Notes on Purchase", 1, 2, 'S', true);
        $pdf->setFont('helvetica', 'I', 10);
        $pdf->Cell(190,10, "Please Make Sure to Pay the Full Amount in the next 3 business days to fulfill the transfer of the Item", 1,0,'C', true);

        $fileNameNew = uniqid('', true).".pdf";
        $filename = 'receipts/'.$fileNameNew;
        $_COOKIE["fileName"] = $filename;
		$pdf->Output("F", $filename, true);
    }

    public function getInstance(){
        return new CustomPDF();
    }
}